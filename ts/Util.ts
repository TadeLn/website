namespace Util {

  export interface AnyObject {
    [index: string|number]: any
  };

  export function mergeObjects(obj1: AnyObject, obj2: AnyObject) {
    for (let key in obj2) {
      if (typeof obj1[key] === "object" && typeof obj2[key] === "object") {
        obj1[key] = mergeObjects(obj1[key], obj2[key]);
      } else {
        obj1[key] = obj2[key];
      }
    }
    return obj1;
  }

  export function deepCopy(v: any) {
    if (typeof v === "object") {
      let newObj: AnyObject = {};
      for (let key in v) {
        newObj[key] = deepCopy(v[key]);
      }
      return newObj;
    } else {
      return v;
    }
  }
}

export default Util;
