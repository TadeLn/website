import fs = require("fs");
import Util from "./Util.js";

interface StringMap {
  [index: string]: string;
}

class Translation {

  private data: any = {};
  private static langs: StringMap = {};

  async load(langName: string) {
    let json: any = await new Promise((resolve, reject) => {
       fs.readFile(`static/lang/${langName}.json`, (err, data) => {
        if (err) {
          reject(err);
        }
        resolve(data);
       });
    });


    let langData = JSON.parse(json);
    let engData = {};
    let engTranslation = Translation.translations.get("eng");
    if (engTranslation !== undefined) {
      engData = engTranslation.getObj()
    } else {
      engData = {};
    }
    this.data = Util.mergeObjects(Util.deepCopy(engData), langData);

    Translation.langs[langName] = this.data.meta.lang.name;
    this.data.meta.lang.this = langName;
    this.data.meta.lang.list = Translation.langs;
  }

  getObj(): object {
    return this.data;
  }



  static translations = new Map<string, Translation>();

  static async add(langName: string) {
    let t = new Translation();
    await t.load(langName);
    this.translations.set(langName, t);
  }

  static get(langName: string) {
    if (Object.keys(this.langs).includes(langName)) {
      return this.translations.get(langName);
    } else {
      return this.translations.get("eng");
    }
  }

  static req(req: any) {
    return Translation.get(req.cookies.lang).getObj();
  }

  private static isInitialized = false;
  static async init() {
    if (this.isInitialized) return;
    this.isInitialized = true;

    const languages = ["eng", "pol", "tok"];
    for (let lang of languages) {
      console.log(`Loading ${lang}`);
      await this.add(lang);
    }
  }

};

Translation.init();

export default Translation;
