import Translation from "./Translation.js";

class Ctx {
  static get(req: any) {
    return {
      url: req.originalUrl,
      t: Translation.req(req)
    }
  }
}

export default Ctx;
