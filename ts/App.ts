import CreateError = require("http-errors");
import Express = require("express");
import Path = require("path");
import CookieParser = require("cookie-parser");
import Logger = require("morgan");

import IndexRouter from "./routes/Index.js";
import SetLanguageRouter from "./routes/SetLanguageRouter.js";
import MeRouter from "./routes/Me.js";
import GamesRouter from "./routes/Games.js";
import ProjectsRouter from "./routes/Projects.js";
import StuffRouter from "./routes/Stuff.js";
import MusicRouter from "./routes/Music.js";

import Translation from "./Translation.js";

const App = Express();

// view engine setup
App.set("views", Path.join(__dirname, "../../views"));
App.set("view engine", "twig");

App.use(Logger("dev"));
App.use(Express.json());
App.use(Express.urlencoded({ extended: false }));
App.use(CookieParser());
App.use(Express.static(Path.join(__dirname, "../../public")));

App.use("/", IndexRouter);
App.use("/setLanguage/", SetLanguageRouter);
App.use("/me/", MeRouter);
App.use("/games/", GamesRouter);
App.use("/projects/", ProjectsRouter);
App.use("/stuff/", StuffRouter);
App.use("/music/", MusicRouter);

// catch 404 and forward to error handler
App.use((req, res, next) => {
  next(CreateError(404));
});

// error handler
App.use((err: any, req: any, res: any) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error", { t: Translation.req(req) });
});

export = App;
