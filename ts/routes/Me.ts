import Express = require("express");
import Ctx from "../Ctx.js";
const router = Express.Router();

router.get("/", (req: any, res: any) => {
  res.render("me", Ctx.get(req));
});

export default router;
