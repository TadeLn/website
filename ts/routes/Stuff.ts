import Express from "express";
import stuff from "../../static/stuff.json";
import Ctx from "../Ctx.js";
const router = Express.Router();

router.get("/", (req: any, res: any) => {
  // Sort all items by most recent modification
  for (let categoryKey of Object.keys(stuff.categories)) {
    let category = (<any> stuff.categories)[categoryKey];
    category.itemsOrder = Object.keys(category.items).sort(
      (keyA, keyB) => {
        const a = category.items[keyA];
        const b = category.items[keyB];
        const timestampA = a.versions.length === 0 ? a.createTimestamp : a.versions[a.versions.length - 1].timestamp;
        const timestampB = b.versions.length === 0 ? b.createTimestamp : b.versions[b.versions.length - 1].timestamp;
        return timestampB - timestampA;
      }
    );
  }


  res.render("stuff", { ...Ctx.get(req), stuff: stuff });
});

export default router;
