import Express from "express";
import games from "../../static/games.json";
import Ctx from "../Ctx.js";
const router = Express.Router();

router.get("/", (req: any, res: any) => {
  res.render("games", { ...Ctx.get(req), games: games });
});

export default router;
