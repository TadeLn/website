import Express = require("express");
import Translation from "../Translation.js";
const router = Express.Router();

router.get("/", (req: any, res: any) => {
  res.cookie("lang", req.query.lang, {
    maxAge: 1000 * 86400 * 3650 // 10 years
  });
  res.redirect(req.query.redirect);
});

export default router;
