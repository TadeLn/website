import Express from "express";
import projects from "../../static/projects.json";
import Ctx from "../Ctx.js";
const router = Express.Router();

router.get("/", (req: any, res: any) => {
  res.render("projects", { ...Ctx.get(req), projects: projects });
});

export default router;
