import Express from "express";
import music from "../../static/music.json";
import Ctx from "../Ctx.js";
const router = Express.Router();

router.get("/", (req: any, res: any) => {
  // Sort all items by release date
  (<any> music).songOrder = Object.keys(music.songs).sort(
    (keyA, keyB) => {
      const a: any = (<any> music.songs)[keyA];
      const b: any = (<any> music.songs)[keyB];
      const timestampA = a.createTimestamp === undefined ? a.versions[a.versions.length - 1].timestamp : a.createTimestamp;
      const timestampB = b.createTimestamp === undefined ? b.versions[b.versions.length - 1].timestamp : b.createTimestamp; 
      return timestampB - timestampA;
    }
  );

  for (let songId of Object.keys(music.songs)) {
    let song = (<any> music.songs)[songId];

    if (song.versions !== undefined) {
      song.versions.sort((a: any, b: any) => a.timestamp - b.timestamp);
    }

    if (song.album !== undefined) {
      let album = (<any> music.albums)[song.album];
      if (album.song === undefined) album.songs = [];
      album.songs[song.track - 1] = songId;
    }
  }


  res.render("music", { ...Ctx.get(req), music: music });
});

export default router;
