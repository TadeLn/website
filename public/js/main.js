function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  let expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  let name = cname + "=";
  let ca = document.cookie.split(";");
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function changeLang(language) {
  setCookie("lang", language, 3650);
  location.reload();
}


function init() {
  let languageSelect = document.getElementById("language-select");
  languageSelect.addEventListener("change", e => {
      changeLang(languageSelect.value);
  });

  let spoilers = document.getElementsByTagName("spoiler");
  for (let spoiler of spoilers) {

    // Add the hidden class (it's not hidden by default because of browsers with disabled javascript)
    spoiler.classList.add("hidden");

    // Make spoilers clickable
    spoiler.addEventListener("click", () => {
      spoiler.classList.remove("hidden");
      spoiler.classList.add("revealed");
    });

    // Add tooltip
    spoiler.classList.add("tooltip");
    let tooltipText = document.createElement("span");
    tooltipText.classList.add("tooltipText");
    tooltipText.innerText = "click to reveal spoiler";
    spoiler.appendChild(tooltipText);
  }


  // Add header anchors
  let headers = document.querySelectorAll("h1, h2, h3, h4, h5, h6");
  for (let header of headers) {
    if (header.id != "") {
      // Create a space between the header and the anchor
      header.appendChild(document.createTextNode(" "));

      let anchor = document.createElement("a");
      anchor.classList.add("header-anchor");
      anchor.href = `#${header.id}`;
      anchor.innerText = "#";
      header.appendChild(anchor);
    }
  }
}

init();
